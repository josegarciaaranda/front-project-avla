import { Component, OnInit } from '@angular/core';
import Task from 'src/app/models/task.model';
import { TaskService } from '../services/task.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task-new',
  templateUrl: './task-new.component.html',
  styleUrls: ['./task-new.component.css']
})
export class TaskNewComponent implements OnInit {

  task: Task;

  constructor(
    private router: Router,
    private taskService: TaskService
  ) { }

  ngOnInit() {
    this.task = {
      id: 0,
      title: '',
      description: '',
      status: ''
    };
  }

  onSaveTask(task: Task) {
    this.taskService.createTask(task).subscribe(res => {
      this.router.navigate(['/admin/Tasks']);
    });
  }

}
