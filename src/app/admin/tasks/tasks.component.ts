import { Component, OnInit } from '@angular/core';

import Task from 'src/app/models/task.model';
import { TaskService } from '../services/task.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  titlesTable: string[] = ['#', 'título', 'descripción', 'estado', 'usarioAsignado', 'Acciones'];
  tasks: Task[];

  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.taskService.getAllTask().subscribe((tasks: Task[]) => {
      this.tasks = tasks;
    });
  }

  onDeleteTask(task: Task) {
    this.taskService.deleteTask(task.id).subscribe(res => {
      this.tasks = this.tasks.filter(prod => prod.id !== task.id);
    });
  }

}
