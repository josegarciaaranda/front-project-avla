import { ProductService } from './product.service';
import Product from '../../models/product.model';
import { of } from 'rxjs';

describe('ProductService', () => {

  let productService: ProductService;
  let httpClientSpy: { get: jasmine.Spy };

  let expectedProducts: Product[];

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    productService = new ProductService(httpClientSpy as any);
    expectedProducts = []
  });

  it('should be created', () => {
    expect(productService).toBeTruthy();
  });

  it('should return 2 products and (HttpClient called one)', () => {
    httpClientSpy.get.and.returnValue(of(expectedProducts));

    productService.getAllProduct().subscribe((data: Product[]) => {
      expect(data.length).toBe(2)
    });

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('should return 1 product by id = 100', () => {
    httpClientSpy.get.and.returnValue(
      of(expectedProducts.filter(item => item.id === 100)[0])
    );

    productService.getProductByid(100).subscribe((product: Product) => {
      expect(product.id).toBe(100);
    });
  });

});
